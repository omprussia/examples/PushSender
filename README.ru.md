# Отправитель push-уведомлений

Приложение предоставляет интерфейс пользователя для отправки push-уведомлений через Аврора Центр.

Приложение может быть использовано как пример реализации интерфейса с сервером push-уведомлений,
так и в качестве утилиты, предназначенной для инфраструктуры тестирования или отладки.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/PushSender/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSender/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/PushSender/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSender/-/commits/dev)  

## Зависимости

+ OpenSSL (для генерации запросов OpenId Connect для получения JWT-token)
+ [pipacs/o2](https://github.com/pipacs/o2) -- библиотека для OAuth 2.0, которая совместима с Qt 5.6 (лицензия: `BSD-2-Clause`). В то время как ванильный Qt>5.8 также имеет внутренний [Qt Network Authorization](https://doc.qt.io/qt-5/qtnetworkauth-index.html), который недоступен в Qt5.6.
+ [QtYAML](https://github.com/uranusjr/qtyaml) -- Qt-оболочка для LibYAML (лицензия: `MIT`)
+ [LibYAML](https://github.com/yaml/libyaml/tree/303b4558ef395f7dcac145e82732728a15e991e7) -- YAML-парсер (лицензия: `MIT`). Используется старая версия (0.1.5), поскольку версия 0.2.5 требует обновления QtYAML

Все зависимости уже скопированы в поддиректорию проекта [3rdparty](3rdparty).

## Использование

На главной странице приложения конфигурируется идентификатор регистрации, который может быть получен из мобильного приложения после регистрации через систему push daemon.

На дополнительной прикреплённой странице конфигурируются параметры push-сервера, которые могут быть получены
из пользовательского веб-интерфейса push-сервера. Настройки сервера сохраняются между перезапусками приложения.

Push-уведомление отправляется из выпадающего меню с текстом из подготовленного поля ввода.

Для настройки демона на устройстве для работы с push-сервером смотрите раздел [Особенности](#особенности).

## Особенности

1. Версии Аврора ОС начиная с 5.0.1.  
Для корректной работы с Push-уведомлениями устройство должно быть зарегистрировано в Аврора Центр. Также для корректной работы необходимо, 
чтобы на устройстве был установлен актуальный для версии ОС клиент Аврора Центр. В этом случае конфиг для push сервиса заполнится автоматически. 

2.  Версии Аврора ОС до 5.0.1.  
Для корректной работы с уведомлениями необходимо настроить push сервис - задать adress, port и выставить флаг для crlValidation в false. 
Для получения текущих настроек, можно воспользоваться командой 
`devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon -m ru.omprussia.PushDaemon.GetNetworkConfiguration`. 
Для установки параметров: `devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon
-m ru.omprussia.PushDaemon.SetNetworkConfiguration "{'address':<'push-server.ru'>,
'port':<8000>,'crlValidation':<false>"}` (важно, чтобы хост был без протокола, например `https://`),
после чего необходимо перезапустить `push-daemon` с помощью команды
`devel-su systemctl restart push-daemon`.


## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

Лицензии зависимостей:
+ [pipacs/o2](https://github.com/pipacs/o2) -- `BSD-2-Clause`;
+ [QtYAML](https://github.com/uranusjr/qtyaml) -- `MIT`;
+ [LibYAML](https://github.com/yaml/libyaml/tree/303b4558ef395f7dcac145e82732728a15e991e7) -- `MIT`.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

Кодекс поведения [(документ на английском)](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

* Файл **[ru.auroraos.PushSender.pro](ru.auroraos.PushSender.pro)** описывает структуру проекта для системы сборки qmake.
* Каталог **[3rdparty](3rdparty)** содержит зависимости проекта.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
  * Каталог **[components](qml/components)** содержит вспомогательные QML компоненты.
  * Каталог **[cover](qml/cover)** содержит реализации обложек приложения.
  * Каталог **[icons](qml/icons)** содержит дополнительные иконки интерфейса пользователя.
  * Каталог **[pages](qml/pages)** содержит страницы приложения.
  * Файл **[PushSender.qml](qml/PushSender.qml)** предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
  * Файл **[ru.auroraos.PushSender.spec](rpm/ru.auroraos.PushSender.spec)** используется инструментом rpmbuild.
* Каталог **[src](src)** содержит исходный код на C++.
  * Файл **[main.cpp](src/main.cpp)** является точкой входа в приложение.
  * Файл **[server.h](src/server.h)** содержит объявление класса пуш сервера.
  * Файл **[server.cpp](src/server.cpp)** содержит определение класса пуш сервера.
  * Файл **[serversettings.h](src/serversettings.h)** содержит настройки сервера.
  * Файл **[settingsmanager.h](src/settingsmanager.h)** содержит объявление менеджера настроек.
  * Файл **[settingsmanager.cpp](src/settingsmanager.cpp)** содержит определение менеджера настроек.
* Каталог **[translations](translations)** содержит файлы перевода интерфейса пользователя.
* Файл **[ru.auroraos.PushSender.desktop](ru.auroraos.PushSender.desktop)** определяет отображение и параметры запуска приложения. 

## Снимки экранов

![screenshots](screenshots/screenshots.png)

## This document in English

- [README.md](README.md)
