################################################################################
##
## Copyright (C) 2021-2022 Open Mobile Platform LLC.
## Contact: https://community.omprussia.ru/open-source
##
## This file is part of the Push Sender project.
##
## Redistribution and use in source and binary forms,
## with or without modification, are permitted provided
## that the following conditions are met:
##
## * Redistributions of source code must retain the above copyright notice,
##   this list of conditions and the following disclaimer.
## * Redistributions in binary form must reproduce the above copyright notice,
##   this list of conditions and the following disclaimer
##   in the documentation and/or other materials provided with the distribution.
## * Neither the name of the copyright holder nor the names of its contributors
##   may be used to endorse or promote products derived from this software
##   without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
## THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
## FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
## IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
## FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
## OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
## PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS;
## OR BUSINESS INTERRUPTION)
## HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
## WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
## EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
################################################################################

INCLUDEPATH += \
    $$PWD/qtyaml/src \
    $$PWD/qtyaml/3rdparty/libyaml/include \
    $$PWD/qtyaml/3rdparty/libyaml/src \

HEADERS += \
    $$PWD/qtyaml/src/qtyaml_global.h \
    $$PWD/qtyaml/src/qyamldocument.h \
    $$PWD/qtyaml/src/qyamlparser.h \
    $$PWD/qtyaml/src/qyamlsequence.h \
    $$PWD/qtyaml/src/qyaml_p.h \
    $$PWD/qtyaml/src/qyamlmapping.h \
    $$PWD/qtyaml/src/qyamlvalue.h \

SOURCES += \
    $$PWD/qtyaml/3rdparty/libyaml/src/api.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/dumper.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/emitter.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/loader.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/parser.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/reader.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/scanner.c \
    $$PWD/qtyaml/3rdparty/libyaml/src/writer.c \
    $$PWD/qtyaml/src/qyamldocument.cpp \
    $$PWD/qtyaml/src/qyamlparser.cpp \
    $$PWD/qtyaml/src/qyamlsequence.cpp \
    $$PWD/qtyaml/src/qyaml.cpp \
    $$PWD/qtyaml/src/qyamlmapping.cpp \
    $$PWD/qtyaml/src/qyamlvalue.cpp \
