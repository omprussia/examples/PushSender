# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Maintainer, 2021-2022
  * Product owner, 2021-2022
  * Reviewer, 2021-2022
* Alexey Andreev, <a.andreev@omp.ru>
  * Developer, 2021-2022
  * Maintainer, 2021-2022
* Pavel Kazeko, <p.kazeko@omp.ru>
  * Developer, 2021-2022
  * Reviewer, 2021-2022
* Vasiliy Rychkov, <v.rychkov@omp.ru>
  * Icon Designer, 2022
* Vladislav Larionov
  * Reviewer, 2023
* Andrey Begichev, <a.begichev@omp.ru>  
  * Developer, 2023
  * Maintainer, 2023
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
