<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="24"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>#descriptionText</source>
        <translation>The app provides a user interface for sending push notifications through Aurora Center</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2022 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>Push Sender</source>
        <translation>Push Sender</translation>
    </message>
</context>
<context>
    <name>KeyValueView</name>
    <message>
        <location filename="../qml/components/KeyValueView.qml" line="66"/>
        <source>Custom data number</source>
        <translation>Custom data number</translation>
    </message>
    <message>
        <location filename="../qml/components/KeyValueView.qml" line="120"/>
        <source>Key</source>
        <translation>Key</translation>
    </message>
    <message>
        <location filename="../qml/components/KeyValueView.qml" line="137"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="34"/>
        <source>Push server settings</source>
        <translation>Push server settings</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>Push Sender</source>
        <translation>Push Sender</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="72"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="79"/>
        <source>API URL</source>
        <translation>API URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="90"/>
        <source>Project ID</source>
        <translation>Project ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="91"/>
        <source>Not configured</source>
        <translation>Not configured</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="99"/>
        <source>Receiver</source>
        <translation>Receiver</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="108"/>
        <source>Registration id after registration on push daemon</source>
        <translation>Registration id after registration on push daemon</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="142"/>
        <source>Answer</source>
        <translation>Answer</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="152"/>
        <source>Send a push notification to get a response</source>
        <translation>Send a push notification to get a response</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="195"/>
        <source>Send push notification</source>
        <translation>Send push notification</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="107"/>
        <source>Registration ID</source>
        <translation>Registration ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="121"/>
        <location filename="../qml/pages/MainPage.qml" line="122"/>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="130"/>
        <location filename="../qml/pages/MainPage.qml" line="131"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
</context>
<context>
    <name>NotificationPage</name>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="46"/>
        <source>Choose</source>
        <translation>Choose</translation>
    </message>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="70"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="71"/>
        <source>Apply new configuration</source>
        <translation>Apply new configuration</translation>
    </message>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="80"/>
        <source>Reject</source>
        <translation>Reject</translation>
    </message>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="81"/>
        <source>Reject new configuration</source>
        <translation>Reject new configuration</translation>
    </message>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="90"/>
        <source>Ignore</source>
        <translation>Ignore</translation>
    </message>
    <message>
        <location filename="../qml/pages/NotificationPage.qml" line="91"/>
        <source>Ignore new configuration</source>
        <translation>Ignore new configuration</translation>
    </message>
</context>
<context>
    <name>SettingsManager</name>
    <message>
        <location filename="../src/settingsmanager.cpp" line="80"/>
        <source>Push Sender</source>
        <translation>Push Sender</translation>
    </message>
    <message>
        <location filename="../src/settingsmanager.cpp" line="81"/>
        <source>Push server configuration</source>
        <translation>Push server configuration</translation>
    </message>
    <message>
        <location filename="../src/settingsmanager.cpp" line="82"/>
        <source>New push server configuration is available</source>
        <translation>New push server configuration is available</translation>
    </message>
    <message>
        <location filename="../src/settingsmanager.cpp" line="87"/>
        <source>Open app</source>
        <translation>Open app</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="102"/>
        <source>API URL</source>
        <translation>API URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="111"/>
        <location filename="../qml/pages/SettingsPage.qml" line="119"/>
        <source>example_1234556890qwertyuiop</source>
        <translation>example_1234556890qwertyuiop</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="127"/>
        <source>public:qWeRtYuIoP</source>
        <translation>public:qWeRtYuIoP</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="134"/>
        <source>Private key</source>
        <translation>Private key</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="135"/>
        <source>-----BEGIN RSA PRIVATE KEY-----...</source>
        <translation>-----BEGIN RSA PRIVATE KEY-----...</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="142"/>
        <source>Audience</source>
        <translation>Audience</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="143"/>
        <location filename="../qml/pages/SettingsPage.qml" line="151"/>
        <source>space separated values</source>
        <translation>space separated values</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="150"/>
        <source>Scope</source>
        <translation>Scope</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="159"/>
        <source>https://someserver.ru/auth/public/oauth2/tokens</source>
        <translation>https://someserver.ru/auth/public/oauth2/tokens</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="78"/>
        <source>Get settings from file...</source>
        <translation>Get settings from file...</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="95"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="94"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="110"/>
        <source>Project ID</source>
        <translation>Project ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="118"/>
        <source>Client ID</source>
        <translation>Client ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="126"/>
        <source>Private key ID</source>
        <translation>Private key ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="158"/>
        <source>Token URL</source>
        <translation>Token URL</translation>
    </message>
</context>
</TS>
