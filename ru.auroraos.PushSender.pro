# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.PushSender

QT += network dbus

CONFIG += \
    auroraapp \
    auroraapp_i18n \

PKGCONFIG += \
    openssl \
    nemonotifications-qt5 \

include(3rdparty/omp-o2.pri)
include(3rdparty/qtyaml.pri)

TRANSLATIONS += \
    translations/ru.auroraos.PushSender.ts \
    translations/ru.auroraos.PushSender-ru.ts \

HEADERS += \
    src/settingsmanager.h \
    src/serversettings.h \
    src/server.h \
    src/utils.h

SOURCES += \
    src/settingsmanager.cpp \
    src/server.cpp \
    src/main.cpp \
    src/utils.cpp

DISTFILES += \
    qml/images/PushSender.svg \
    qml/components/CustomTextField.qml \
    qml/components/KeyValueView.qml \
    qml/cover/DefaultCoverPage.qml \
    qml/pages/AboutPage.qml \
    qml/pages/MainPage.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/NotificationPage.qml \
    qml/PushSender.qml \
    rpm/ru.auroraos.PushSender.spec \
    translations/*.ts \
    ru.auroraos.PushSender.desktop \
    LICENSE.BSD-3-CLAUSE.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    AUTHORS.md \
    README.md \
    README.ru.md

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
