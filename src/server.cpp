// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtCore/QDebug>
#include <QtCore/QVariantMap>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <QtCore/QDateTime>
#include <QtCore/QFile>
#include <QtNetwork/QNetworkReply>

#include "o0globals.h"
#include "o0settingsstore.h"

#include "settingsmanager.h"
#include "server.h"

#include "utils.h"

namespace
{
    QJsonObject pushHeader(const QString &privateKeyId)
    {
        QJsonObject header;
        header["alg"] = "RS256"; // алгоритм шифрования
        header["kid"] = privateKeyId;
        return header;
    }

    QJsonObject pushPayload(const QString &clientId, const QString &tokenUrl)
    {
        QJsonObject payload;
        payload["iss"] = clientId;
        payload["sub"] = clientId;
        payload["iat"] = utils::currentSecsSinceEpoch();
        payload["exp"] = utils::currentSecsSinceEpoch() + 600; // токен действителен 10 минут
        payload["jti"] = utils::genUniqToken(36); // уникальный идентификатор токена длиной 36 символов
        payload["aud"] = tokenUrl;
        return payload;
    }
}

Server::Server(SettingsManager *settingsManager, QObject *parent)
    : O2(parent)
{
    if (settingsManager) {
        m_serverSettings = settingsManager->serverSettings();

        connect(settingsManager, &SettingsManager::serverSettingsChanged, this,
                &Server::onServerSettingsChangedAction);
    }

    connect(this, &Server::linkingSucceeded, this, &Server::onLinkingSucceededAction);
}

void Server::onServerSettingsChangedAction(const ServerSettings &serverSettings)
{
    m_serverSettings = serverSettings;
}

void Server::onLinkingSucceededAction()
{
    QJsonObject body{
        { QStringLiteral("target"), m_regId },
        { QStringLiteral("ttl"), QStringLiteral("2h") },
        { QStringLiteral("type"), QStringLiteral("device") },
        { QStringLiteral("notification"),
          { { { QStringLiteral("title"), m_title },
              { QStringLiteral("message"), m_message },
              { QStringLiteral("data"), QJsonObject::fromVariantMap(m_customData) } } } }
    };

    QNetworkRequest sendPushRequest(requestUrl());
    sendPushRequest.setRawHeader(QByteArrayLiteral("Authorization"),
                                 QStringLiteral("Bearer %1").arg(token()).toUtf8());

    QNetworkReply *sendPushReply = manager_->post(
            sendPushRequest,
            QJsonDocument(body).toJson(QJsonDocument::Compact).toStdString().c_str());

    timedReplies_.add(sendPushReply);

    connect(
            sendPushReply, &QNetworkReply::finished, this,
            [this]() {
                QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
                QByteArray replyData = reply->readAll();
                if (reply->error() == QNetworkReply::NoError) {
                    emit serverAnswer(replyData);
                } else {
                    qWarning() << Q_FUNC_INFO << reply->errorString();
                    emit serverError(QStringLiteral("%1: %2")
                                             .arg(reply->errorString())
                                             .arg(QString(replyData)));
                }
            },
            Qt::QueuedConnection);
}

void Server::sendPushMessage(const QString &regId, const QString &title, const QString &message,
                             const QVariantMap &customData)
{
    m_regId = regId;
    m_title = title;
    m_message = message;
    m_customData = customData;

    setTokenUrl(m_serverSettings.tokenUrl);
    setRefreshTokenUrl(m_serverSettings.tokenUrl);
    setRequestUrl(QStringLiteral("%1projects/%2/messages")
                          .arg(m_serverSettings.apiUrl, m_serverSettings.projectId));
    setClientSecret(m_serverSettings.privateKey);
    setScope(m_serverSettings.scope);
    setGrantType(QStringLiteral("client_credentials"));

    O0SettingsStore *store = new O0SettingsStore(O2_ENCRYPTION_KEY);
    store->setGroupKey(QStringLiteral("aurorapushserver"));
    setStore(store);
    setLinked(false);

    link();
}

void Server::link()
{
    if (linked()) {
        qDebug() << Q_FUNC_INFO << QStringLiteral("Linked already");
        emit linkingSucceeded();
        return;
    }

    setLinked(false);
    setToken(QStringLiteral(""));
    setTokenSecret(QStringLiteral(""));
    setExtraTokens(QVariantMap());
    setRefreshToken(QStringLiteral());
    setExpires(0);

    QUrlQuery urlQuery;

    urlQuery.addQueryItem(QStringLiteral("grant_type"),
                          QStringLiteral("client_credentials"));

    urlQuery.addQueryItem(QStringLiteral("cliend_id"),
                          m_serverSettings.clientId);

    urlQuery.addQueryItem(QStringLiteral("client_assertion_type"),
                          QStringLiteral("urn:ietf:params:oauth:client-assertion-type:jwt-bearer"));

    urlQuery.addQueryItem(QStringLiteral("client_assertion"),
                          utils::createJwt(
                              pushHeader(m_serverSettings.privateKeyId),
                              pushPayload(m_serverSettings.clientId, tokenUrl_.url()),
                              m_serverSettings.privateKey
                          )
    );

    urlQuery.addQueryItem(QStringLiteral("audience"),
                          m_serverSettings.audience);

    urlQuery.addQueryItem(QStringLiteral("scope"),
                          m_serverSettings.scope);

    QNetworkRequest tokenRequest(tokenUrl_);
    tokenRequest.setHeader(QNetworkRequest::ContentTypeHeader,
                           QStringLiteral("application/x-www-form-urlencoded"));

    QNetworkReply *tokenReply = manager_->post(tokenRequest, urlQuery.toString(QUrl::FullyEncoded).toUtf8());
    timedReplies_.add(tokenReply);

    connect(tokenReply, &QNetworkReply::finished, this, &Server::onTokenReplyFinished,
            Qt::QueuedConnection);
    connect(tokenReply,
            static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(
                    &QNetworkReply::error),
            this, &Server::onTokenReplyError, Qt::QueuedConnection);
}

void Server::onTokenReplyFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QByteArray replyData = reply->readAll();
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument doc = QJsonDocument::fromJson(replyData);
        const QJsonObject rootObject = doc.object();

        QVariantMap reply;
        for (const QString &key : rootObject.keys())
            reply.insert(key, rootObject[key].toVariant());

        setToken(reply.value(QStringLiteral("access_token")).toString());
        setExpires(reply.value(QStringLiteral("expires_in")).toInt());
        setLinked(true);
        emit linkingSucceeded();
    } else {
        qWarning() << Q_FUNC_INFO << reply->errorString();
        emit serverError(
                QStringLiteral("%1: %2").arg(reply->errorString()).arg(QString(replyData)));
    }
}
