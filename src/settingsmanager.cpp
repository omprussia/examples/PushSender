// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QSettings>
#include <QtCore/QFileSystemWatcher>
#include <QtCore/QStandardPaths>
#include <QtGui/QGuiApplication>
#include <auroraapp.h>

#include "qyamldocument.h"
#include "qyamlmapping.h"

#include "settingsmanager.h"

SettingsManager::SettingsManager(QObject *parent)
    : QObject(parent),
      m_appConfFile(QDir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation))
                            .absoluteFilePath(QStringLiteral("settings"))),
      m_scanRootPath(Aurora::Application::organizationPathTo(QStringLiteral("")).toLocalFile()),
      m_scanConfPath(
              QStringLiteral("%1/%2").arg(m_scanRootPath, QStringLiteral("PushSenderConfig"))),
      m_scanConfFile(
              QStringLiteral("%1/%2").arg(m_scanConfPath, QStringLiteral("configuration.yaml")))
{
    QScopedPointer<QSettings> settings(new QSettings(m_appConfFile, QSettings::IniFormat));
    settings->beginGroup(QStringLiteral("CommonSettings"));
    m_serverSettingsDateTime = settings->value(QStringLiteral("updateDateTime")).value<QDateTime>();
    settings->endGroup();
    settings->beginGroup(QStringLiteral("ServerSettings"));
    m_serverSettings.apiUrl = settings->value(QStringLiteral("apiUrl")).value<QString>();
    m_serverSettings.tokenUrl = settings->value(QStringLiteral("tokenUrl")).value<QString>();
    m_serverSettings.clientId = settings->value(QStringLiteral("clientId")).value<QString>();
    m_serverSettings.projectId = settings->value(QStringLiteral("projectId")).value<QString>();
    m_serverSettings.privateKeyId =
            settings->value(QStringLiteral("privateKeyId")).value<QString>();
    m_serverSettings.privateKey = settings->value(QStringLiteral("privateKey")).value<QString>();
    m_serverSettings.audience = settings->value(QStringLiteral("audience")).value<QString>();
    m_serverSettings.scope = settings->value(QStringLiteral("scope")).value<QString>();
    settings->endGroup();
    settings->beginGroup(QStringLiteral("CustomSettings"));
    for (const auto &key : settings->allKeys())
        m_customSettings[key] = settings->value(key);
    settings->endGroup();

    this->watcher = new QFileSystemWatcher(this);
    updateWatchedPaths(false);

    connect(watcher, &QFileSystemWatcher::fileChanged, this,
            &SettingsManager::updateWatchedPathsWithNotification);
    connect(watcher, &QFileSystemWatcher::directoryChanged, this,
            &SettingsManager::updateWatchedPathsWithNotification);
}

void SettingsManager::updateWatchedPathsWithNotification()
{
    updateWatchedPaths(true);
}

void SettingsManager::updateWatchedPaths(bool showNotification)
{
    QDateTime dateTime;
    QDir().exists(m_scanRootPath) ? watcher->addPath(m_scanRootPath)
                                  : watcher->removePath(m_scanRootPath);
    QDir().exists(m_scanConfPath) ? watcher->addPath(m_scanConfPath)
                                  : watcher->removePath(m_scanConfPath);
    if (QDir().exists(m_scanConfFile)) {
        const QFileInfo fileInfo(m_scanConfFile);
        dateTime = qMax(fileInfo.created(), fileInfo.lastModified());

        if (dateTime > m_serverSettingsDateTime && dateTime != m_newServerSettingsDateTime) {
            m_newServerSettingsDateTime = dateTime;

            if (qApp->applicationState() == Qt::ApplicationInactive && showNotification) {
                static QString defaultActionName = QStringLiteral("default");
                m_notification.reset(new Notification());
                m_notification->setAppName(tr("Push Sender"));
                m_notification->setSummary(tr("Push server configuration"));
                m_notification->setBody(tr("New push server configuration is available"));
                m_notification->setIsTransient(false);
                m_notification->setItemCount(1);
                m_notification->setHintValue("x-nemo-feedback", "sms_exists");
                m_notification->setRemoteAction(
                        Notification::remoteAction(defaultActionName, tr("Open app")));
                m_notification->publish();

                connect(m_notification.data(), &Notification::actionInvoked, this,
                        [this](const QString &name) {
                            if (name.compare(defaultActionName) == 0) {
                                emit raiseViewRequest();
                            }
                        });
            }

            if (!m_newServerSettingsAvailable) {
                m_newServerSettingsAvailable = true;
                emit newServerSettingsAvailableChanged(true);
            }
        }

        watcher->addPath(m_scanConfFile);
    } else {
        watcher->removePath(m_scanConfFile);
    }
}

bool SettingsManager::newServerSettingsAvailable() const
{
    return m_newServerSettingsAvailable;
}

void SettingsManager::newServerSettingsApply()
{
    m_notification.reset();

    if (!m_newServerSettingsAvailable)
        return;

    m_newServerSettingsAvailable = false;
    emit newServerSettingsAvailableChanged(false);

    m_serverSettingsForceUpdate = true;
    setServerSettings(serverSettingsFromFile(m_scanConfFile));
}

void SettingsManager::newServerSettingsReject()
{
    m_notification.reset();

    if (!m_newServerSettingsAvailable)
        return;

    m_newServerSettingsAvailable = false;
    emit newServerSettingsAvailableChanged(false);

    m_serverSettingsForceUpdate = true;
    setServerSettings(m_serverSettings);
}

void SettingsManager::newServerSettingsIgnore()
{
    m_notification.reset();

    if (!m_newServerSettingsAvailable)
        return;

    m_newServerSettingsAvailable = false;
    emit newServerSettingsAvailableChanged(false);
}

ServerSettings SettingsManager::serverSettingsFromFile(const QString &filePath) const
{
    ServerSettings serverSettings;

    if (!filePath.endsWith(QStringLiteral(".yml")) && !filePath.endsWith(QStringLiteral(".yaml")))
        return serverSettings;

    QFile inputFile(filePath);
    if (!inputFile.open(QIODevice::ReadOnly))
        return serverSettings;

    QByteArray yaml = inputFile.readAll();
    QtYAML::DocumentList docs = QtYAML::Document::fromYaml(yaml);
    QtYAML::Mapping mapping =
            docs.first().mapping()[QStringLiteral("push_notification_system")].toMapping();

    serverSettings.apiUrl = mapping[QStringLiteral("api_url")].toString();
    serverSettings.tokenUrl = mapping[QStringLiteral("token_url")].toString();
    serverSettings.clientId = mapping[QStringLiteral("client_id")].toString();
    serverSettings.projectId = mapping[QStringLiteral("project_id")].toString();
    serverSettings.privateKeyId = mapping[QStringLiteral("key_id")].toString();
    serverSettings.privateKey = mapping[QStringLiteral("private_key")].toString();
    serverSettings.audience = mapping[QStringLiteral("audience")].toString();
    serverSettings.scope = mapping[QStringLiteral("scopes")].toString();

    return serverSettings;
}

ServerSettings SettingsManager::serverSettings() const
{
    return m_serverSettings;
}

void SettingsManager::setServerSettings(const ServerSettings &serverSettings)
{
    if (m_serverSettings == serverSettings && !m_serverSettingsForceUpdate)
        return;

    m_serverSettings = serverSettings;
    m_serverSettingsDateTime = QDateTime::currentDateTime();
    m_serverSettingsForceUpdate = false;
    emit serverSettingsChanged(serverSettings);

    QScopedPointer<QSettings> settings(new QSettings(m_appConfFile, QSettings::IniFormat));
    settings->beginGroup(QStringLiteral("CommonSettings"));
    settings->setValue(QStringLiteral("updateDateTime"), m_serverSettingsDateTime);
    settings->endGroup();
    settings->beginGroup(QStringLiteral("ServerSettings"));
    settings->setValue(QStringLiteral("apiUrl"), serverSettings.apiUrl);
    settings->setValue(QStringLiteral("tokenUrl"), serverSettings.tokenUrl);
    settings->setValue(QStringLiteral("clientId"), serverSettings.clientId);
    settings->setValue(QStringLiteral("projectId"), serverSettings.projectId);
    settings->setValue(QStringLiteral("privateKeyId"), serverSettings.privateKeyId);
    settings->setValue(QStringLiteral("privateKey"), serverSettings.privateKey);
    settings->setValue(QStringLiteral("audience"), serverSettings.audience);
    settings->setValue(QStringLiteral("scope"), serverSettings.scope);
    settings->endGroup();
    settings->sync();
}

QVariant SettingsManager::customSetting(const QString &key, const QVariant &defaultValue) const
{
    return m_customSettings.value(key, defaultValue);
}

void SettingsManager::setCustomSetting(const QString &key, const QVariant &value)
{
    if (m_customSettings.contains(key) && m_customSettings.value(key) == value)
        return;

    m_customSettings[key] = value;
    emit customSettingChanged(key, value);

    QScopedPointer<QSettings> settings(new QSettings(m_appConfFile, QSettings::IniFormat));
    settings->beginGroup(QStringLiteral("CustomSettings"));
    settings->setValue(key, value);
    settings->endGroup();
    settings->sync();
}
