// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SERVERSETTINGS_H
#define SERVERSETTINGS_H

#include <QtCore/QObject>

struct ServerSettings
{
    Q_GADGET

    Q_PROPERTY(QString apiUrl MEMBER apiUrl)
    Q_PROPERTY(QString tokenUrl MEMBER tokenUrl)
    Q_PROPERTY(QString clientId MEMBER clientId)
    Q_PROPERTY(QString projectId MEMBER projectId)
    Q_PROPERTY(QString privateKeyId MEMBER privateKeyId)
    Q_PROPERTY(QString privateKey MEMBER privateKey)
    Q_PROPERTY(QString audience MEMBER audience)
    Q_PROPERTY(QString scope MEMBER scope)

public:
    inline bool operator==(const ServerSettings &rhs) const
    {
        return apiUrl == rhs.apiUrl && tokenUrl == rhs.tokenUrl && clientId == rhs.clientId
                && projectId == rhs.projectId && privateKeyId == rhs.privateKeyId
                && privateKey == rhs.privateKey && audience == rhs.audience && scope == rhs.scope;
    }
    inline bool operator!=(const ServerSettings &rhs) const { return !(*this == rhs); }

public:
    QString apiUrl{ QString::null };
    QString tokenUrl{ QString::null };
    QString clientId{ QString::null };
    QString projectId{ QString::null };
    QString privateKeyId{ QString::null };
    QString privateKey{ QString::null };
    QString audience{ QString::null };
    QString scope{ QString::null };
};

#endif // SERVERSETTINGS_H
