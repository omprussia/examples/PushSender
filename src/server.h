// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QObject>

#include "serversettings.h"
#include "o2.h"

class SettingsManager;

class Server : public O2
{
    Q_OBJECT

public:
    explicit Server(SettingsManager *settingsManager, QObject *parent = nullptr);

    Q_INVOKABLE void sendPushMessage(const QString &regId, const QString &title,
                                     const QString &message,
                                     const QVariantMap &customData = QVariantMap());

signals:
    void serverError(const QString &serverError);
    void serverAnswer(const QString &serverAnswer);

protected slots:
    void link() override;
    void onTokenReplyFinished() override;

private slots:
    void onServerSettingsChangedAction(const ServerSettings &serverSettings);
    void onLinkingSucceededAction();

private:
    ServerSettings m_serverSettings{};
    QString m_regId{ QString::null };
    QString m_title{ QString::null };
    QString m_message{ QString::null };
    QVariantMap m_customData{};
};

#endif // SERVER_H
