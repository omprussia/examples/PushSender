#ifndef UTILS_H
#define UTILS_H

#include <QString>

class QDateTime;
class QByteArray;
class QJsonObject;

namespace utils
{
    qint64 currentSecsSinceEpoch();

    QString base64UrlEncode(const QByteArray &input);

    QString genUniqToken(size_t strLength);

    QByteArray rsaSign(const QByteArray &data, const QByteArray &privateKeyPem);

    QString createJwt(const QJsonObject &header, const QJsonObject &payload, const QString &secretKey);
}

#endif // UTILS_H
