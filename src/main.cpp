// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGui/QGuiApplication>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <auroraapp.h>

#include "settingsmanager.h"
#include "server.h"

int main(int argc, char *argv[])
{
    qRegisterMetaType<ServerSettings>("ServerSettings");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("PushSender"));

    QScopedPointer<SettingsManager> settingsManager(new SettingsManager());
    QScopedPointer<Server> server(new Server(settingsManager.data()));

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->rootContext()->setContextProperty("pushServer", server.data());
    view->rootContext()->setContextProperty("settingsManager", settingsManager.data());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/PushSender.qml")));
    view->show();

    QObject::connect(settingsManager.data(), &SettingsManager::raiseViewRequest, [&view]() {
        view->raise();
        view->showFullScreen();
    });

    return application->exec();
}
