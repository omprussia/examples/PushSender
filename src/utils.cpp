#include "utils.h"

#include <openssl/pem.h>
#include <openssl/bio.h>

#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

namespace utils
{
    qint64 currentSecsSinceEpoch()
    {
        QDateTime utcDateTime = QDateTime::currentDateTimeUtc();
        return utcDateTime.toTime_t();
    }

    QString base64UrlEncode(const QByteArray &input)
    {
        QString encoded = QString::fromLatin1(input.toBase64().data());
        encoded.replace('+', '-');
        encoded.replace('/', '_');
        encoded.remove('=');
        return encoded;
    }

    QString genUniqToken(size_t strLength)
    {
        static const QString possibleCharacters(
                    QStringLiteral("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
        );

        qsrand(static_cast<unsigned int>(QTime::currentTime().msec())); // NOTE: QRandomGenerator in future Qt versions

        QString randomString;
        for (size_t i = 0; i < strLength; ++i) {
            qint32 index = qrand() % possibleCharacters.length();
            QChar nextChar = possibleCharacters.at(index);
            randomString.append(nextChar);
        }

        return randomString;
    }

    QByteArray rsaSign(const QByteArray &data, const QByteArray &privateKeyPem) {
        RSA *rsa = nullptr;
        BIO *bio = BIO_new_mem_buf(privateKeyPem.constData(), privateKeyPem.size());
        if (!bio) {
            qWarning() << "Failed to create BIO";
            return QByteArray();
        }

        rsa = PEM_read_bio_RSAPrivateKey(bio, &rsa, nullptr, nullptr);
        BIO_free(bio);

        if (!rsa) {
            qWarning() << "Failed to load RSA private key";
            return QByteArray();
        }

        unsigned char hash[SHA256_DIGEST_LENGTH];
        SHA256((const unsigned char *)data.constData(), data.size(), hash);

        unsigned char *signature = nullptr;
        size_t signatureLen = RSA_size(rsa);
        signature = (unsigned char *)malloc(signatureLen);

        if (RSA_sign(NID_sha256, hash, SHA256_DIGEST_LENGTH, signature, (unsigned int *)&signatureLen, rsa)) {
            QByteArray result((char *)signature, signatureLen);
            free(signature);
            RSA_free(rsa);
            return result;
        } else {
            free(signature);
            RSA_free(rsa);
            qWarning() << "Failed to sign data";
            return QByteArray();
        }
    }

    QString createJwt(const QJsonObject &header, const QJsonObject &payload, const QString &secretKey)
    {
        QJsonDocument headerDoc(header);
        QJsonDocument payloadDoc(payload);

        const QByteArray headerJson = headerDoc.toJson(QJsonDocument::Compact);
        const QByteArray payloadJson = payloadDoc.toJson(QJsonDocument::Compact);

        const QString encodedHeader = base64UrlEncode(headerJson);
        const QString encodedPayload = base64UrlEncode(payloadJson);

        const QString stringToSign = encodedHeader + "." + encodedPayload;

        const QByteArray signature = rsaSign(stringToSign.toUtf8(), secretKey.toUtf8());
        const QString encodedSignature = base64UrlEncode(signature);

        return QString("%1.%2.%3").arg(encodedHeader, encodedPayload, encodedSignature);
    }
}
