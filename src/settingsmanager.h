// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QDateTime>
#include <QtCore/QVariant>
#include <QtCore/QFileSystemWatcher>
#include <nemonotifications-qt5/notification.h>

#include "serversettings.h"

struct NotificationDeleter
{
    static inline void cleanup(Notification *notification)
    {
        if (notification) {
            notification->close();
            notification->deleteLater();
        }
    }
};

class SettingsManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool newServerSettingsAvailable READ newServerSettingsAvailable NOTIFY
                       newServerSettingsAvailableChanged)

public:
    explicit SettingsManager(QObject *parent = nullptr);

    bool newServerSettingsAvailable() const;
    Q_INVOKABLE void newServerSettingsApply();
    Q_INVOKABLE void newServerSettingsReject();
    Q_INVOKABLE void newServerSettingsIgnore();

    Q_INVOKABLE ServerSettings serverSettingsFromFile(const QString &filePath) const;
    Q_INVOKABLE ServerSettings serverSettings() const;
    Q_INVOKABLE void setServerSettings(const ServerSettings &serverSettings);

    Q_INVOKABLE QVariant customSetting(const QString &key, const QVariant &defaultValue) const;
    Q_INVOKABLE void setCustomSetting(const QString &key, const QVariant &value);

signals:
    void newServerSettingsAvailableChanged(bool newServerSettingsAvailable);
    void serverSettingsChanged(const ServerSettings &serverSettings);
    void customSettingChanged(const QString &key, const QVariant &value);
    void raiseViewRequest();

private slots:
    void updateWatchedPaths(bool showNotification);
    void updateWatchedPathsWithNotification();

private:
    const QString m_appConfFile{};
    const QString m_scanRootPath{};
    const QString m_scanConfPath{};
    const QString m_scanConfFile{};

    bool m_serverSettingsForceUpdate{ false };
    bool m_newServerSettingsAvailable{ false };
    QScopedPointer<Notification, NotificationDeleter> m_notification{ nullptr };
    QDateTime m_serverSettingsDateTime{};
    QDateTime m_newServerSettingsDateTime{};
    ServerSettings m_serverSettings{};
    QHash<QString, QVariant> m_customSettings{};
    QFileSystemWatcher *watcher;
};

#endif // SETTINGSMANAGER_H
