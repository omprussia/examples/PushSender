# Push Sender

The app provides a user interface for sending push notifications via Aurora Center.

It can be used both as an implementation example of interaction with a push notification server
and for utilitarian purposes for infrastructure testing or debugging.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/PushSender/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSender/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/PushSender/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/PushSender/-/commits/dev)  

## Dependencies

+ OpenSSL (to generate the requests of OpenId Connect to get a JWT-token)
+ [pipacs/o2](https://github.com/pipacs/o2) -- a library for OAuth 2.0, which is compatible with Qt 5.6 (license: `BSD-2-Clause`). While the vanilla Qt>5.8 also have internal [Qt Network Authorization](https://doc.qt.io/qt-5/qtnetworkauth-index.html), it's not available in Qt5.6.
+ [QtYAML](https://github.com/uranusjr/qtyaml) -- Qt wrapper for the LibYAML (license: `MIT`)
+ [LibYAML](https://github.com/yaml/libyaml/tree/303b4558ef395f7dcac145e82732728a15e991e7) -- YAML parser (license: `MIT`). The old one version is used (0.1.5), since version 0.2.5 requires QtYAML update

All the dependencies are already copied to the [3rdparty](3rdparty) subdirectory of the project.

## Usage

On the main page of the application, the registration id is configured, which is received from
the mobile application after the registration via the system push daemon.

On the additional attached page, the push server parameters are configured, which can be obtained
from the web-UI interface of the push server. Server settings are saved between application restarts.

A push message is sent from the pulley menu with the text from the prepared input field.

To configure the daemon on the device to work with the push server, see [Features](#features).

## Features
1. Aurora OS versions starting with 5.0.1.  
Device must be registered in Aurora Center for correct operation with Push notifications. Also for correct operation it is necessary, 
Aurora Center client, which is up-to-date for the OS version, must be installed on the device. In this case the config for push service will be filled automatically. 
2.  Aurora OS versions up to 5.0.1.  
For correct work with notifications it is necessary to configure the push service - set adress, port and set the flag for crlValidation to false. 
To get the current settings, you can use the command 
`devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon -m ru.omprussia.PushDaemon.GetNetworkConfiguration`. 
To set the parameters: `devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon
-m ru.omprussia.PushDaemon.SetNetworkConfiguration “{‘address’:<‘push-server.ru’>,
'port':<8000>,'crlValidation':<false>"}` (it is important that the host is protocol-less, for example `https://`),
after which `push-daemon` must be restarted using the command
`devel-su systemctl restart push-daemon`.  

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The licenses of the dependencies:
+ [pipacs/o2](https://github.com/pipacs/o2) -- `BSD-2-Clause`;
+ [QtYAML](https://github.com/uranusjr/qtyaml) -- `MIT`;
+ [LibYAML](https://github.com/yaml/libyaml/tree/303b4558ef395f7dcac145e82732728a15e991e7) -- `MIT`.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure
of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.PushSender.pro](ru.auroraos.PushSender.pro)** file describes the project structure for the qmake build system.
* **[3rdparty](3rdparty)** directory contains dependencies.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[components](qml/components)** directory contains the custom QML components.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[icons](qml/icons)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[PushSender.qml](qml/PushSender.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.PushSender.spec](rpm/ru.auroraos.PushSender.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
  * **[server.h](src/server.h)** file contains declaration push server.
  * **[server.cpp](src/server.cpp)** file contains definition push server.
  * **[serversettings.h](src/serversettings.h)** file contains server settings.
  * **[settingsmanager.h](src/settingsmanager.h)** file contains a settings manager declaration.
  * **[settingsmanager.cpp](src/settingsmanager.cpp)** file contains a settings manager definition.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.PushSender.desktop](ru.auroraos.PushSender.desktop)** file defines the display and parameters for launching the application.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
