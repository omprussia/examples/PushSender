// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0

TextField {
    id: textField

    property string propertyName

    objectName: "textField"
    width: parent.width
    hideLabelOnEmptyField: false
    font.pixelSize: Theme.fontSizeExtraSmall
    text: propertyName ? settingsManager.customSetting(propertyName, "") : ""
    rightItem: IconButton {
        objectName: "clearButton"
        width: icon.width
        height: icon.height
        icon.source: "image://theme/icon-m-input-clear"
        opacity: textField.text.length > 0 ? 1.0 : 0.0

        Behavior on opacity  {
            objectName: "behaviorOnOpacity"

            FadeAnimation {
                objectName: "fadeAnimation"
            }
        }

        onClicked: textField.text = ""
    }

    onTextChanged: {
        if (propertyName) {
            settingsManager.setCustomSetting(propertyName, text);
        }
    }
}
