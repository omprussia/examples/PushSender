// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

Rectangle {
    id: keyValueView

    property var pairs: ({})

    readonly property int _pairsNumber: 10
    readonly property bool _lightOnDark: palette.colorScheme === Theme.LightOnDark

    function updatePairs() {
        var pairsTemp = {};
        for (var i = 0; i < pairsRepeater.count; ++i) {
            var key = pairsRepeater.itemAt(i).key;
            var value = pairsRepeater.itemAt(i).value;
            pairsTemp[key] = value;
        }
        pairs = pairsTemp;
    }

    objectName: "keyValueView"
    anchors {
        left: parent.left
        right: parent.right
        leftMargin: Theme.horizontalPageMargin
        rightMargin: Theme.horizontalPageMargin
    }
    height: column.height
    radius: Theme.paddingSmall
    color: Theme.rgba(_lightOnDark ? Theme.darkPrimaryColor : Theme.lightPrimaryColor, Theme.opacityLow)
    border {
        width: Theme.dp(1)
        color: Theme.rgba(_lightOnDark ? Theme.lightPrimaryColor : Theme.darkPrimaryColor, Theme.opacityHigh)
    }
    layer.enabled: true
    layer.effect: OpacityMask {
        objectName: "opacityMask"
        maskSource: Item {
            objectName: "opacityMaskSource"
            width: keyValueView.width
            height: keyValueView.height

            Rectangle {
                objectName: "opacityMaskRect"
                anchors.fill: parent
                radius: keyValueView.radius
            }
        }
    }

    Column {
        id: column

        objectName: "column"
        width: parent.width

        ComboBox {
            id: pairsNumber

            objectName: "pairsNumber"
            label: qsTr("Custom data number")
            width: parent.width
            menu: ContextMenu {
                objectName: "menu"

                Repeater {
                    objectName: "repeater"
                    model: _pairsNumber
                    delegate: MenuItem {
                        objectName: "item%1".arg(model.index)
                        text: model.index
                    }
                }
            }

            onCurrentIndexChanged: settingsManager.setCustomSetting("pairsNumber", currentIndex)
            Component.onCompleted: currentIndex = settingsManager.customSetting("pairsNumber", 0)
        }

        Column {
            objectName: "pairsColumn"
            width: parent.width
            bottomPadding: pairsNumber.currentIndex === 0 ? 0 : Theme.paddingLarge

            Repeater {
                id: pairsRepeater

                objectName: "pairsRepeater"
                model: pairsNumber.currentIndex
                delegate: pairDelegate
            }
        }
    }

    Component {
        id: pairDelegate

        Item {
            property alias key: keyField.text
            property alias value: valueField.text

            objectName: "customDataItem"
            width: parent.width
            height: Math.max(keyField.height, valueField.height)

            CustomTextField {
                id: keyField

                objectName: "keyField%1".arg(modelData)
                propertyName: "key%1".arg(modelData)
                anchors.left: parent.left
                width: parent.width / 5 * 2
                textRightMargin: Theme.paddingMedium
                labelVisible: false
                placeholderText: qsTr("Key")

                onTextChanged: {
                    settingsManager.setCustomSetting(propertyName, text);
                    updatePairs();
                }
            }

            CustomTextField {
                id: valueField

                objectName: "valueField%1".arg(modelData)
                propertyName: "value%1".arg(modelData)
                anchors.right: parent.right
                width: parent.width - keyField.width
                textLeftMargin: Theme.paddingMedium
                labelVisible: false
                placeholderText: qsTr("Value")

                onTextChanged: {
                    settingsManager.setCustomSetting(propertyName, text);
                    updatePairs();
                }
            }
        }
    }
}
