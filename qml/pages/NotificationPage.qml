// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Dialog {
    objectName: "notificationPage"
    allowedOrientations: Orientation.All
    canAccept: switchedArea.currentSwitch !== undefined && switchedArea.currentSwitch !== null
    backNavigation: false

    onAccepted: {
        if (switchedArea.currentSwitch === applySwitch) {
            settingsManager.newServerSettingsApply();
        } else if (switchedArea.currentSwitch === rejectSwitch) {
            settingsManager.newServerSettingsReject();
        } else if (switchedArea.currentSwitch === ignoreSwitch) {
            settingsManager.newServerSettingsIgnore();
        }
    }

    SilicaFlickable {
        id: flickableArea

        objectName: "flickableArea"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: column

            objectName: "column"
            width: parent.width
            spacing: Theme.paddingMedium
            bottomPadding: Theme.paddingLarge

            DialogHeader {
                objectName: "pageHeader"
                cancelText: ""
                acceptText: qsTr("Choose")
            }

            Column {
                id: switchedArea

                property var currentSwitch

                function updateCurrentSwitch(object) {
                    if (switchedArea.currentSwitch !== object) {
                        if (switchedArea.currentSwitch) {
                            switchedArea.currentSwitch.checked = false;
                        }
                        switchedArea.currentSwitch = object;
                        switchedArea.currentSwitch.checked = true;
                    }
                }

                width: parent.width
                spacing: Theme.paddingMedium

                TextSwitch {
                    id: applySwitch

                    text: qsTr("Apply")
                    description: qsTr("Apply new configuration")
                    automaticCheck: false

                    onClicked: switchedArea.updateCurrentSwitch(applySwitch)
                }

                TextSwitch {
                    id: rejectSwitch

                    text: qsTr("Reject")
                    description: qsTr("Reject new configuration")
                    automaticCheck: false

                    onClicked: switchedArea.updateCurrentSwitch(rejectSwitch)
                }

                TextSwitch {
                    id: ignoreSwitch

                    text: qsTr("Ignore")
                    description: qsTr("Ignore new configuration")
                    automaticCheck: false

                    onClicked: switchedArea.updateCurrentSwitch(ignoreSwitch)
                }
            }
        }
    }
}
