// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    objectName: "mainPage"
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: flickableArea

        objectName: "flickableArea"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: buttonsLayout.top
        }
        contentHeight: column.height
        clip: true

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        PullDownMenu {
            objectName: "menu"

            MenuItem {
                objectName: "pushServerSettingsMenuItem"
                text: qsTr("Push server settings")

                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        }

        Column {
            id: column

            objectName: "column"
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingSmall

            PageHeader {
                objectName: "pageHeader"
                title: qsTr("Push Sender")
                extraContent.children: [
                    IconButton {
                        objectName: "pageHeaderButton"
                        anchors.verticalCenter: parent.verticalCenter
                        icon {
                            source: "image://theme/icon-m-about"
                            sourceSize {
                                width: Theme.iconSizeMedium
                                height: Theme.iconSizeMedium
                            }
                        }

                        onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                    }
                ]
            }

            SectionHeader {
                objectName: "serverSection"
                text: qsTr("Server")
            }

            TextField {
                id: apiUrlField

                objectName: "apiUrlField"
                label: qsTr("API URL")
                placeholderText: "Not configured"
                font.pixelSize: Theme.fontSizeExtraSmall
                text: applicationWindow.pushServerSettings.apiUrl
                readOnly: true
            }

            TextField {
                id: projectIdField

                objectName: "projectIdField"
                label: qsTr("Project ID")
                placeholderText: qsTr("Not configured")
                font.pixelSize: Theme.fontSizeExtraSmall
                text: applicationWindow.pushServerSettings.projectId
                readOnly: true
            }

            SectionHeader {
                objectName: "receiverSection"
                text: qsTr("Receiver")
            }

            CustomTextField {
                id: registarationIdField

                objectName: "registarationIdField"
                propertyName: "registrationId"
                label: qsTr("Registration ID")
                placeholderText: qsTr("Registration id after registration on push daemon")
            }

            SectionHeader {
                objectName: "notificationSection"
                text: qsTr("Notification")
            }

            CustomTextField {
                id: titleField

                objectName: "titleField"
                propertyName: "title"
                label: qsTr("Title")
                placeholderText: qsTr("Title")
            }

            CustomTextField {
                id: messageField

                objectName: "messageField"
                propertyName: "message"
                label: qsTr("Message")
                placeholderText: qsTr("Message")
            }

            KeyValueView {
                id: keyValueView

                objectName: "keyValueView"
            }

            SectionHeader {
                objectName: "answerSection"
                text: qsTr("Answer")
            }

            TextArea {
                id: answerTextArea

                objectName: "answerTextArea"
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: TextEdit.Wrap
                text: qsTr("Send a push notification to get a response")
                readOnly: true

                Connections {
                    objectName: "pushServerConnections"
                    target: pushServer

                    onServerError: answerTextArea.text = serverError
                    onServerAnswer: answerTextArea.text = serverAnswer
                }
            }
        }
    }

    OpacityRampEffect {
        sourceItem: flickableArea
        direction: OpacityRamp.TopToBottom
        offset: flickableArea.height / (flickableArea.height + Theme.paddingMedium)
        slope: flickableArea.height / (flickableArea.height * (1.0 - offset)) - 1.0
        enabled: flickableArea.height < column.height
    }

    Column {
        id: buttonsLayout

        objectName: "buttonsLayout"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        topPadding: Theme.paddingLarge
        bottomPadding: Theme.paddingLarge

        Button {
            id: sendPushBtn

            objectName: "sendPushMenuItem"
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingLarge
            }
            text: qsTr("Send push notification")

            onClicked: pushServer.sendPushMessage(registarationIdField.text, titleField.text, messageField.text, keyValueView.pairs)
        }
    }
}
