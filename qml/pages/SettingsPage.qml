// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import "../components"

Dialog {
    property var _settingsCur: applicationWindow.pushServerSettings
    property var _settingsNew
    property alias _apiUrl: apiUrlField.text
    property alias _projectId: projectIdField.text
    property alias _clientId: clientIdField.text
    property alias _privateKeyId: privateKeyIdField.text
    property alias _privateKey: privateKeyField.text
    property alias _audience: audienceField.text
    property alias _scope: scopeField.text
    property alias _tokenUrl: tokenUrlField.text

    objectName: "settingsPage"
    allowedOrientations: Orientation.All
    canAccept: _apiUrl && _projectId && _clientId && _privateKeyId && _privateKey && _audience && _scope && _tokenUrl && (_settingsCur.apiUrl !== _apiUrl || _settingsCur.projectId !== _projectId || _settingsCur.clientId !== _clientId || _settingsCur.privateKeyId !== _privateKeyId || _settingsCur.privateKey !== _privateKey || _settingsCur.audience !== _audience || _settingsCur.scope !== _scope || _settingsCur.tokenUrl !== _tokenUrl)

    on_SettingsCurChanged: _settingsNew = _settingsCur
    on_SettingsNewChanged: {
        _apiUrl = _settingsNew.apiUrl;
        _projectId = _settingsNew.projectId;
        _clientId = _settingsNew.clientId;
        _privateKeyId = _settingsNew.privateKeyId;
        _privateKey = _settingsNew.privateKey;
        _audience = _settingsNew.audience;
        _scope = _settingsNew.scope;
        _tokenUrl = _settingsNew.tokenUrl;
    }
    onAccepted: {
        _settingsNew.apiUrl = _apiUrl;
        _settingsNew.projectId = _projectId;
        _settingsNew.clientId = _clientId;
        _settingsNew.privateKeyId = _privateKeyId;
        _settingsNew.privateKey = _privateKey;
        _settingsNew.audience = _audience;
        _settingsNew.scope = _scope;
        _settingsNew.tokenUrl = _tokenUrl;
        settingsManager.setServerSettings(_settingsNew);
    }

    Component {
        id: filePickerPage

        FilePickerPage {
            objectName: "filePickerPage"
            nameFilters: ['*.yml', '*.yaml']

            onSelectedContentPropertiesChanged: {
                var filePath = selectedContentProperties.filePath;
                _settingsNew = settingsManager.serverSettingsFromFile(filePath);
            }
        }
    }

    SilicaFlickable {
        id: flickableArea

        objectName: "flickableArea"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        PullDownMenu {
            objectName: "menu"

            MenuItem {
                objectName: "getSettingsMenuItem"
                text: qsTr("Get settings from file...")

                onClicked: pageStack.push(filePickerPage)
            }
        }

        Column {
            id: column

            objectName: "column"
            width: parent.width
            spacing: Theme.paddingMedium
            bottomPadding: Theme.paddingLarge

            DialogHeader {
                objectName: "pageHeader"
                cancelText: qsTr("Back")
                acceptText: qsTr("Apply")
            }

            CustomTextField {
                id: apiUrlField

                objectName: "apiUrlField"
                label: qsTr("API URL")
                placeholderText: "https://someserver.ru/push/public/api/v1/"
            }

            CustomTextField {
                id: projectIdField

                objectName: "projectIdField"
                label: qsTr("Project ID")
                placeholderText: qsTr("example_1234556890qwertyuiop")
            }

            CustomTextField {
                id: clientIdField

                objectName: "clientIdField"
                label: qsTr("Client ID")
                placeholderText: qsTr("example_1234556890qwertyuiop")
            }

            CustomTextField {
                id: privateKeyIdField

                objectName: "privateKeyIdField"
                label: qsTr("Private key ID")
                placeholderText: qsTr("public:qWeRtYuIoP")
            }

            CustomTextField {
                id: privateKeyField

                objectName: "privateKeyField"
                label: qsTr("Private key")
                placeholderText: qsTr("-----BEGIN RSA PRIVATE KEY-----...")
            }

            CustomTextField {
                id: audienceField

                objectName: "audienceField"
                label: qsTr("Audience")
                placeholderText: qsTr("space separated values")
            }

            CustomTextField {
                id: scopeField

                objectName: "scopeField"
                label: qsTr("Scope")
                placeholderText: qsTr("space separated values")
            }

            CustomTextField {
                id: tokenUrlField

                objectName: "tokenUrlField"
                label: qsTr("Token URL")
                placeholderText: qsTr("https://someserver.ru/auth/public/oauth2/tokens")
            }
        }
    }
}
