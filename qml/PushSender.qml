// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow {
    id: applicationWindow

    property var pushServerSettings: settingsManager.serverSettings()

    objectName: "applicationWindow"
    allowedOrientations: defaultAllowedOrientations
    cover: Qt.resolvedUrl("cover/DefaultCoverPage.qml")
    initialPage: Component {
        MainPage {
        }
    }

    Component.onCompleted: {
        if (settingsManager.newServerSettingsAvailable) {
            pageStack.push(Qt.resolvedUrl("pages/NotificationPage.qml"), {}, PageStackAction.Immediate);
        }
    }

    Connections {
        objectName: "settingsManagerConnections"
        target: settingsManager

        onServerSettingsChanged: pushServerSettings = serverSettings
        onNewServerSettingsAvailableChanged: {
            if (newServerSettingsAvailable) {
                pageStack.push(Qt.resolvedUrl("pages/NotificationPage.qml"));
            }
        }
    }
}
